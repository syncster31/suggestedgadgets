---
title: "Cheap Laptops for Autocad"
date: 2018-06-14T22:53:07+08:00
draft: false
---
Are you looking for cheap laptops for your startup projects? Do you need to use Autodesk's Autocad software but you have a limited budget? Well, you are in the right place.

In this article, I have summarized a few **cheap and very affordable** laptops that you can use to your **Autocad** drawing projects. Please note that these laptops are not the best for the job but they are very decent in terms of *price against performance*.

Just because they're cheap doesn't mean you can't use them to do *heavy duty* jobs. I have compiled this list with the *minimum system requirements* of Autodesk's AutoCAD in mind. The system requirements are achieved in this list with prices of just a little over **$200** dollars! Can you imagine that? Instead of buying those laptops with over a thousand bucks prices, if you're just starting, you can use any of these cheap laptops at the moment and then when you have the budget and maybe making more money with you projects, then you can afford the more powerful and *expensive devices*.

---
---

Minimum System Requirements
---
The minimum system requirements set by Autodesk is as follows:

| Particulars | Values |
| ----------- | -------|
Operating System | Microsoft Windows 10 (64-bit only) (version 1607 and up recommended)
|    | Microsoft® Windows® 7 SP1 (32-bit & 64-bit)
|    | Microsoft Windows 8.1 with Update KB2919355 (32-bit & 64-bit)
CPU Type	| 32-bit: 1 gigahertz (GHz) or faster 32-bit (x86) processor
|    | 64-bit: 1 gigahertz (GHz) or faster 64-bit (x64) processor
Memory |	32-bit: 2 GB (4 GB recommended)
|    | 64-bit: 4 GB (8 GB recommended)
Display Resolution |	Conventional Displays:
|    | 1360 x 768 (1920 x 1080 recommended) with True Color
|    | High Resolution & 4K Displays:
|    | Resolutions up to 3840 x 2160 supported on Windows 10, 64 bit systems (with capable display card)

---
---

With the above table as the basis for selecting the laptops, I carefully pick those with the price range of around **200 bucks**. One of them, I used for my Autocad projects and just works perfectly. Without further saying, please see the list below for **laptops for autocad**:


### <a target="_blank" href="https://www.amazon.com/gp/product/B07BLPHRX9/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07BLPHRX9&linkCode=as2&tag=syncster31-20&linkId=00cb7b90939e523603351b77b37ab450">1. Dell I3565-A453BLK-PUS</a>

<a target="_blank"  href="https://www.amazon.com/gp/product/B07BLPHRX9/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07BLPHRX9&linkCode=as2&tag=syncster31-20&linkId=089b6319d497827aba9f579545e9889b"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=US&ASIN=B07BLPHRX9&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=syncster31-20" style="width: 500px !important;"></a>

With the new **Dell I3565**, you can work on Autocad smoothly as long as you are using it wisely. How wisely? I'll tell you later. I'll tell you some tips on how to maximize the use of these laptops efficiently. Dell I3565 comes with a 15.6" screen display, allowing you to view your drawing elegantly and nice in the eyes. Not too big and not too small as well. To further compare the units, I will just summarize in table each of the system specifications.

| Particulars | Values |
| ----------- | -------|
OS | Windows 10 Home
CPU Type | 2.4 GHz
Memory RAM | 4 GB
Screen Size | 15.6"
Display Resolution | 1366 x 768
Price | **$254.49**

<a target="_blank"  href="https://www.amazon.com/gp/product/B07BLPHRX9/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07BLPHRX9&linkCode=as2&tag=syncster31-20&linkId=00cb7b90939e523603351b77b37ab450">Click here for more info</a>


### <a target="_blank"  href="https://www.amazon.com/gp/product/B078YCMD67/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B078YCMD67&linkCode=as2&tag=syncster31-20&linkId=9ac4cc1901252f3b2990eee093333686">2. HP 15.6" Laptop, AMD A6-9220</a>

<a target="_blank"  href="https://www.amazon.com/gp/product/B078YCMD67/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B078YCMD67&linkCode=as2&tag=syncster31-20&linkId=2c3222084e7583f6ec3f1c7df8186d4e"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=US&ASIN=B078YCMD67&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=syncster31-20"  style="width: 500px !important;"></a>

| Particulars | Values |
| ----------- | -------|
OS | Windows 10 Home
CPU Type | 2.5 GHz
Memory RAM | 4 GB SD RAM
Screen Size | 15.6"
Display Resolution | 1366 x 768
Price | **$294.86**

<a target="_blank"  href="https://www.amazon.com/gp/product/B078YCMD67/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B078YCMD67&linkCode=as2&tag=syncster31-20&linkId=9ac4cc1901252f3b2990eee093333686">Click here for more info</a>


### <a target="_blank"  href="https://www.amazon.com/gp/product/B01M1AQ316/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B01M1AQ316&linkCode=as2&tag=syncster31-20&linkId=4c10c8c0515dd29e511af3e4e3d04321">3. HP Elitebook 8470p</a>


<a target="_blank"  href="https://www.amazon.com/gp/product/B01M1AQ316/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B01M1AQ316&linkCode=as2&tag=syncster31-20&linkId=c9885ca24a391c0c86c41be68b1b6e68"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=US&ASIN=B01M1AQ316&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=syncster31-20" style="width: 500px !important;"></a>

| Particulars | Values |
| ----------- | -------|
OS | Windows 10
CPU Type | 2.6 GHz
Memory RAM | 8 GB SD RAM
Screen Size | 14.1"
Display Resolution | 1366 x 768
Price | **$237.44**

<a target="_blank"  href="https://www.amazon.com/gp/product/B01M1AQ316/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B01M1AQ316&linkCode=as2&tag=syncster31-20&linkId=4c10c8c0515dd29e511af3e4e3d04321">Click here for more info</a>


### <a target="_blank"  href="https://www.amazon.com/gp/product/B077ZHV9WQ/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B077ZHV9WQ&linkCode=as2&tag=syncster31-20&linkId=880f913ec40f0249df25c02f2f0ead6b">4. Lenovo ideapad 320</a>


<a target="_blank"  href="https://www.amazon.com/gp/product/B077ZHV9WQ/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B077ZHV9WQ&linkCode=as2&tag=syncster31-20&linkId=d0e028535dbb19e3be5e5f483b1af564"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=US&ASIN=B077ZHV9WQ&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=syncster31-20" style="width: 500px !important;"></a>


| Particulars | Values |
| ----------- | -------|
OS | Windows 10 Home
CPU Type | 1.6 GHz
Memory RAM | 4 GB SD RAM
Screen Size | 15.6"
Display Resolution | 1366 x 768
Price | **$259.43**

<a target="_blank"  href="https://www.amazon.com/gp/product/B077ZHV9WQ/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B077ZHV9WQ&linkCode=as2&tag=syncster31-20&linkId=880f913ec40f0249df25c02f2f0ead6b">Click here for more info</a>



---
---
Now that I presented four cheapest laptops you can use for Autocad, it's up to you to decide. What I've picked are those that in a near range of 200 - 300 dollars. It's up to you to compare RAM and Prices. Even the screen size will be based on your preference. Now the tips.

Tips
---

- Make Autocad a Priority

-- To make sure everything works as smooth as possible, make sure that when you run Autocad, you are not running multiple instances of different applications. Specially heavy apps like photoshop or torrent client apps as they take a lot of bandwidth and resources.

- Keep your PC Clean

-- You have to make sure that your pc is always clean from worms, viruses and malwares. These things will definitely slow down your PC without you even noticing them.

- Up to Date

-- Always update your security apps especially that you're using Windows if you purchase one of these laptops.
-- Update firmwares and drivers as needed. This will ensure that you avoid possible vulnerabilities from malwares and other viruses.

---
---
I hope you enjoy reading and have been enlightened a little bit on the subject. Thank you.
